package com.GrandMAStudio.Frame.service;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class FrameServiceTest {
    private FrameService frameService = new FrameService();

    @Test
    public void testFillMatrixExceptedIsOk() {
        int[][] matrix = new int[5][5];
        int picWidth = 2;
        int picHeight = 2;
        int picX = 1;
        int picY = 1;
        frameService.fillMatrix(matrix, picWidth, picHeight, picX, picY);

        int[][] exceptedResult = {{0, 0, 0, 0, 0},
                {0, 7, 7, 0, 0},
                {0, 7, 7, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0}};

        assertTrue(Arrays.deepEquals(matrix, exceptedResult));
    }

    @Test
    public void testOffsetAreaExceptedisOk() {
        int[][] matrix = new int[5][5];
        int picWidth = 2;
        int picHeight = 2;
        int picX = 1;
        int picY = 1;
        frameService.fillMatrix(matrix, picWidth, picHeight, picX, picY);

        frameService.offsetArea(matrix, picX, picY);

        int[][] exceptedResult = {{7, 7, 0, 0, 0},
                {7, 7, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0}};

        assertTrue(Arrays.deepEquals(matrix, exceptedResult));
    }

}
