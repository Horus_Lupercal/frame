package com.GrandMAStudio.Frame.service;

import com.GrandMAStudio.Frame.exception.PicSizeException;

public class FrameService {
    private int endHeight;
    private int endWeight;


    public void offsetArea(int[][] matrix, int picX, int picY) {
        for (int x = 0; x < matrix.length; x++) {
            for (int y = 0; y < matrix[0].length; y++) {
                if (picX <= x && picY <= y && endHeight > x && endWeight > y) {
                    int temp = matrix[x][y];
                    matrix[x][y] = matrix[x - picX][y - picY];
                    matrix[x - picX][y - picY] = temp;
                }
            }
        }
    }

    public void fillMatrix(int[][] matrix, int picWidth, int picHeight, int picX, int picY) {
        if (checkPoints(matrix, picWidth, picHeight, picX, picY)) {
            throw new PicSizeException(picWidth, picHeight);
        }

        for (int x = 0; x < matrix.length; x++) {
            for (int y = 0; y < matrix[0].length; y++) {
                if (picX <= x && picY <= y && endHeight > x && endWeight > y) {
                    matrix[x][y] = 7;
                }
            }
        }
    }

    private boolean checkPoints(int[][] matrix, int picWidth, int picHeight, int picX, int picY) {
        endHeight = picHeight + picX;
        endWeight = picWidth + picY;

        return picWidth < 0 || picWidth >= matrix[0].length
                || picHeight < 0 || picHeight >= matrix.length
                || picX < 0 || picX >= matrix[0].length
                || picY < 0 || picY >= matrix.length
                || endHeight > matrix.length || endWeight > matrix[0].length;
    }
}
