package com.GrandMAStudio.Frame.exception;

public class PicSizeException extends RuntimeException {
    public PicSizeException(int picWidth, int picHeight) {
        super(String.format(
                "The object with follow size: Height=%d, Width=%d cant exist ot the frame.", picHeight, picWidth));
    }
}
