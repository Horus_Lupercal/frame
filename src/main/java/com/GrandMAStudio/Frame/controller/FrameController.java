package com.GrandMAStudio.Frame.controller;

import com.GrandMAStudio.Frame.exception.PicSizeException;
import com.GrandMAStudio.Frame.service.FrameService;
import com.GrandMAStudio.Frame.view.FrameView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

public class FrameController {
    private FrameService service;
    private FrameView frameView;

    public FrameController(FrameService service, FrameView frameView) {
        this.service = service;
        this.frameView = frameView;
    }

    public void initialization() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Enter frame Width :");
            int frameWidth = Integer.parseInt(reader.readLine());
            System.out.println("Enter frame Height :");
            int frameHeight = Integer.parseInt(reader.readLine());
            int[][] matrix = new int[frameHeight][frameWidth];

            System.out.println("Enter pic Width :");
            int picWidth = Integer.parseInt(reader.readLine());
            System.out.println("Enter pic Height :");
            int picHeight = Integer.parseInt(reader.readLine());

            System.out.println("Enter pic X:");
            int picX = Integer.parseInt(reader.readLine());
            System.out.println("Enter pic Y:");
            int picY = Integer.parseInt(reader.readLine());

            service.fillMatrix(matrix, picWidth, picHeight, picX, picY);

            frameView.PrintMatrix(arraysToStringArray(matrix));

            service.offsetArea(matrix, picX, picY);

            frameView.PrintMatrix(arraysToStringArray(matrix));

        } catch (IOException e) {
            e.printStackTrace();
        } catch (PicSizeException e) {
            System.err.println(e.getMessage());
        } catch (Exception e) {
            System.err.println("Wrong input format");
        }
    }

    private String[][] arraysToStringArray(int[][] matrix) {
        String[][] result = new String[matrix.length][matrix[0].length];
        for (int x = 0; x < matrix.length; x++) {
            for (int y = 0; y < matrix[0].length; y++) {
                result[x][y] = String.valueOf(matrix[x][y]);
            }
        }
        return result;
    }

}
