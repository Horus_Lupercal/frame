package com.GrandMAStudio.Frame;

import com.GrandMAStudio.Frame.controller.FrameController;
import com.GrandMAStudio.Frame.service.FrameService;
import com.GrandMAStudio.Frame.view.FrameView;

public class ApplicationClass {

    public static void main(String[] args) {
        FrameController controller = new FrameController(new FrameService(), new FrameView());
        controller.initialization();
    }
}
