package com.GrandMAStudio.Frame.view;

import javax.swing.*;
import java.awt.*;

public class FrameView {

    public void PrintMatrix(Object[][] matrix) {
        for (int x = 0; x < matrix.length; x++) {
            for (int y = 0; y < matrix[0].length; y++) {
                System.out.print(matrix[x][y]);
            }
            System.out.println();
        }
        System.out.println("==============================");
    }
}
